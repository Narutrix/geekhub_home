package home_work_6;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ActionTest
 */
public class ActionTest extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ActionTest() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String message = request.getParameter("message");
		String action = request.getParameter("action");
		String name = request.getParameter("name");
		String value = request.getParameter("value");
		String scope = request.getParameter("scope");

		HttpSession session = request.getSession();
		ServletContext sc = request.getServletContext();

		if ("sessison".equals(scope)) {
			if ("add".equals(action) || "update".equals(action)) {
				session.setAttribute(name, value);
			} else if ("remove".equals(action)) {
				session.removeAttribute(name);
			}
		} else {
			if ("add".equals(action) || "update".equals(action)) {
				sc.setAttribute(name, value);
			} else if ("remove".equals(action)) {
				sc.removeAttribute(name);
			}
		}
		response.getWriter().write("<form action=\"./Index.html\" \">");
		response.getWriter().write("<input type=\"submit\" value=\"back\"");
		response.getWriter().write("</form>");
		PrintWriter out = response.getWriter();
		out.println("<p> Message : " + message + "</p>");
        
        out.println("<p><h4>Session contest:</h4></p>");
 
        
        Enumeration<String> names = session.getAttributeNames();
        while(names.hasMoreElements()) {
                String atribute = names.nextElement();
                out.println("<p>" + atribute + " : " + session.getAttribute(atribute) + "</p>");
        }
        
        response.getWriter().println("<p><h4>Servlet contest:</h4></p>");
        names = sc.getAttributeNames();
        while(names.hasMoreElements()) {
                String atribute = names.nextElement();
                out.println("<p>" + atribute + " : " +sc.getAttribute(atribute) + "</p>");
        }
        
        out.println("</body></html>");

		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
