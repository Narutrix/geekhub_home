import java.net.*;
import java.io.*;

public class Client {
        public static void main(String [] args) throws UnknownHostException, IOException {
                String ip = args[0];
                Socket socket = new Socket(ip, 1026);
                PrintStream out = new PrintStream( socket.getOutputStream());
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
                boolean flag = true;
                while(flag) {
                        String message = console.readLine();
                        out.println(message);
                        if(message.equalsIgnoreCase("off")) {
                                flag = false;
                        }
                }
                socket.close();
        }
}