import java.net.*;
import java.util.*;
import java.util.regex.*;
import java.io.*;

public class URLConection {

	
	public static void main(String[] args) {
		try {
			URLConnection conect = new URL("http://progbook.ru").openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					conect.getInputStream()));
			String str= "";
			ArrayList<Integer> array = new ArrayList<>();

			while ((str = in.readLine()) != null) {
				Pattern pattern = Pattern.compile("<a href=.*[^>]>.*");
				Matcher matcher = pattern.matcher(str);
				if(matcher.matches()){
					int end =str.indexOf('>');
					String str1;
					str1 = str.substring(matcher.start(), end+1).replaceAll("<a href=\"(.*[^>])\">.*", "$1");
	
					HttpURLConnection c = (HttpURLConnection) new URL(str1).openConnection();
					array.add(c.getResponseCode());
					System.out.println(array);
				}
			}
				
			
			in.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
