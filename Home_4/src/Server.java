import java.net.*;
import java.io.*;

public class Server {

        @SuppressWarnings("resource")
        public static void main(String [] args) {
                try {
                        ServerSocket serverSocket = new ServerSocket(1026);
                        Socket socket;
                        socket = serverSocket.accept();
                        BufferedReader in = new BufferedReader(new InputStreamReader( socket.getInputStream()));
                        OutputStream out = socket.getOutputStream();
                        boolean flag = true;
                        while(flag) {
                                String message = in.readLine();
                                if(message.equalsIgnoreCase("off")) {
                                        flag = false;
                                }
                                System.out.println(message);
                        }
                        socket.close();
                } catch (IOException e) {
                        System.out.println(e.getMessage());
                }
        }
}