import java.util.*;
import java.util.concurrent.TimeUnit;
import java.io.*;
import java.util.regex.*;

public class SearchThread extends Thread {

	static Scanner in = new Scanner(System.in);
	private static ArrayList list = new ArrayList();
	private static ArrayList ans = new ArrayList();
	private int start;
	private int end;
	private static String pattern;

	public static void search(ArrayList list2, File StartDirectory) {
		File[] lists = StartDirectory.listFiles();
		if (lists == null)
			return;
		for (int i = 0; i < lists.length; i++) {
			if (lists[i].isDirectory()) {
				search(list, lists[i]);
			} else {
				list.add(lists[i]);
			}
		}
	}

	public void run() {
			for (int i = start; i <= end; i++) {
			String str = list.get(i).toString();
			String str1 ="" ;
			str1 = str.substring(str.length() - 4, str.length());
			if (str1.equalsIgnoreCase(pattern)) {
				ans.add(list.get(i));
			}
		}
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (Exception e) {

		}
	}

	SearchThread(int from, int endd) {
		start = from;
		end = endd;
	}

	public static void main(String[] args) throws InterruptedException {
		long timestart = System.currentTimeMillis();
	    pattern = in.next();
		File StartDirectory = new File("D:/Brut");
		search(list, StartDirectory);
		int size = list.size();
		SearchThread myThread1 = new SearchThread(0, list.size() / 2);
		SearchThread myThread2 = new SearchThread(list.size() / 2 + 1,
				list.size() - 1);
		myThread1.start();
		myThread2.start();
		myThread1.join();
		myThread2.join();
		System.out.println(list.size());
		long timeend = System.currentTimeMillis();
		System.out.println("Copying time: " + (timeend - timestart)
				+ " milliseconds.");
		for(Object s: ans) {
			System.out.println(s);
		}
	}
}