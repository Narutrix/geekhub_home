import java.sql.*;
import java.util.*;

public class DataBase {

	public static Scanner in = new Scanner(System.in);

	static Connection connecttobase() throws SQLException {
		String url = "jdbc:mysql://";
		System.out.print("host: ");
		String host = in.nextLine();
		System.out.print("login: ");
		String login = in.nextLine();
		System.out.print("password: ");
		String password = in.nextLine();
		System.out.print("database: ");
		String database = in.nextLine();
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(url + host + "/"
					+ database, login, password);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return connection;
	}

	static void databasequery(Connection connecttobase) throws SQLException {
		System.out.print("Enter query: ");
		String query = in.nextLine();
		Statement statement = connecttobase.createStatement();
		if ("SELECT".equalsIgnoreCase(query)) {
			ResultSet res;
			try {
				res = statement.executeQuery(query);
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				return;
			}
			ResultSetMetaData data = res.getMetaData();
			int colum = data.getColumnCount();
			int[] sizecolum = new int[colum];
			for (int i = 0; i < colum; i++) {
				sizecolum[i] = data.getColumnName(i + 1).length();
			}
			while (res.next()) {
				for (int i = 0; i < colum; i++)
					sizecolum[i] = Math.max(sizecolum[i], res.getString(i + 1)
							.length());
			}
			
			for (int i = 0; i < colum; i++) {
				String namecolum = data.getColumnName(i + 1);
				System.out.print(namecolum);

				int space = 0;
				while (namecolum.length() + space <= sizecolum[i]) {
					space++;
					System.out.print(" ");

				}
				System.out.print("|");
			}
			System.out.println();
			}
			
			else
			{
				int changed;
				try
				{
					changed = statement.executeUpdate( query );
				}
				catch ( SQLException e )
				{
					System.out.println( e.getMessage() );
					return;
				}
				System.out.println( "Lines changed: " + changed );
		}
	}
	public static void main(String[] args) throws SQLException {
		Connection connecttobase = connecttobase();
		if ( connecttobase == null ) 
			return;
		String request;
		do
		{
			databasequery( connecttobase );
			System.out.println( "another request ( yes/no )" );
			request = in.nextLine();
		} while ( request.equalsIgnoreCase( "yes" ) );
		connecttobase.close();

	}

}
