import java.io.*;

public class copyfile {

	public static void main(String[] args) throws IOException {
		long timestart = System.currentTimeMillis();
		if (args[0].equalsIgnoreCase("a")) {
			BufferedInputStream filein = new BufferedInputStream(
					new FileInputStream(args[1]));
			BufferedOutputStream fileout = new BufferedOutputStream(
					new FileOutputStream(args[2]));
			try {
				byte[] b = new byte[2048];
				int c = 0;
				while ((c = filein.read(b)) >= 0) {
					fileout.write(b, 0, c);
				}
			} catch (IOException e) {
				System.out.println(e.getMessage());
			} finally {
				fileout.close();
				filein.close();
			}
		}
		if(args[0].equalsIgnoreCase("b")) {
            InputStream filein = new FileInputStream(args[1]);
            OutputStream fileout = new FileOutputStream(args[2]);
            try {
                    int c = 0;
                    while ((c = filein.read()) >= 0) {
                            fileout.write(c);
                    }
            } catch (IOException e) {
                    System.out.println(e.getMessage());
            } finally {
                    fileout.close();
                    filein.close();
            }
    }

		long timeend = System.currentTimeMillis();
		System.out.println("Copying time: " + (timeend - timestart)
				+ " milliseconds.");
	}

}
