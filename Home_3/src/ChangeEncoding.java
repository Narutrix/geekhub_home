import java.io.*;
import java.util.*;

public class ChangeEncoding {

	static Scanner in = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
		System.out.println("Enter encoding");
		String encod = in.next();
		Reader reader = new InputStreamReader(new FileInputStream("input.txt"));
		Writer writer = new OutputStreamWriter(new FileOutputStream(
				"output.txt"), encod);
		try {
			int c = 0;
			while ((c = reader.read()) >= 0)
				writer.write(c);
		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			System.out.println(e.getMessage());
		} finally {
			reader.close();
			writer.close();

		}
	}

}
