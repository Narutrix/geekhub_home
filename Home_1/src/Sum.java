import java.util.*;

public class Sum {
	    static Scanner in = new Scanner(System.in);   
        public static void main(String [] args) {
                double n = octdechex(in.next());
                double m = octdechex(in.next());
                System.out.println(n+m);
        }
        
        public static double octdechex(String str) {
                int odh = 10;
                int p = 1;
                if(str.charAt(0) == '-'){
                	str = str.substring(1);
                	p *= -1; 
                }
                if(str.charAt(0) == '0') {
                        if((str.length() != 1) && (str.charAt(1)=='x')) {
                                odh = 16;
                                str = str.substring(2);
                        } else {
                                odh = 8;
                                str = str.substring(1);
                        }
                }
                
                int pointposition = str.length();
                for(int i = 0; i<str.length(); i++) {
                        if(str.charAt(i)=='.') {
                                pointposition = i;
                        }
                }
                
               
                double w = 1.0;
                double Result = 0.0;
                for(int i =0; pointposition-1 >= i; i++) {
                	if(str.charAt(i) <= '9'){
                		Result += (str.charAt(i) - '0')*w;
                		
                	}
                	else{
                		Result += (str.charAt(i) - 'A'+10 )*w;
                		
                	}
                        w *= odh;
                }
                
                w = odh;
                for(int i = pointposition+1; i<str.length(); i++) {
                	if(str.charAt(i) >= '9'){
                		Result += (str.charAt(i) - 'A' + 10) / w;	
                	}
                	else{
                		Result += (str.charAt(i) - '0') / w;
                		
                	}
                        w *= odh;
                }
                
                return Result *p;
        }
}