import java.util.*;

public class FractionSequence {
	static Scanner in = new Scanner(System.in);
	public static void main(String[] args) {
		System.out.println("Enter n");
		int n = in.nextInt();
		ArrayList< Double > fraction = new ArrayList < Double >();
		for(int i = 1; i <= n; i++){
	         fraction.add(1.0/i);
	    }
		for ( int i=0; i<n; i++){
			System.out.print(fraction.get(i) +  " ");
		}
	}
}