import java.util.*;


public class FibonacciSequence {
	static Scanner in = new Scanner(System.in);
	public static void main (String args[]){
		System.out.println("Enter n");
		int n = in.nextInt();
		ArrayList< Integer > fibonaci = new ArrayList < Integer >();
		int a=0, b=1, sum_fib;
		for(int i = 0; i < n; i++){
	         sum_fib = a + b;
	         a = b;
	         b = sum_fib;
	         fibonaci.add(sum_fib);
	     }
		for ( int i=0; i<n; i++){
			System.out.print(fibonaci.get(i) +  " ");
		}
	}
}