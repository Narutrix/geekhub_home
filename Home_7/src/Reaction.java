import java.io.IOException;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.catalina.Session;
/**
 * Servlet implementation class Action
 */
public class Reaction extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Reaction() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	//@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String value = request.getParameter("value");
		String action = request.getParameter("action");
		
		HttpSession session = request.getSession();

		if ("add".equalsIgnoreCase(action)) {
			Enumeration<String> enumstr = session.getAttributeNames();
			boolean flag = false;
			while (enumstr.hasMoreElements() && !flag) {
				String temp = enumstr.nextElement();
				if (temp.equals(name)) {
					flag = true;
					List<String> attribute = (List<String>) session
							.getAttribute(name);
					if (!attribute.contains(value)) {
						attribute.add(value);
					}
				}
			}
			if (!flag) {
				List<String> list = new ArrayList<String>();
				list.add(value);
				session.setAttribute(name, list);
			}
		} else if ("remove".equalsIgnoreCase(action)) {
			List<String> list = (List<String>) session.getAttribute(name);
			if (list.size() == 1) {
				session.removeAttribute(name);
			} else {
				list.remove(value);
			}
		}
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
