<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title></title>
</head>
<body>
        <form action="./Reaction" method="get">
        <table border="1">
        <tr>
        <th width="100"><c:out value="Name"/></th>
        <th width="100"><c:out value="Value"/></th>
        <th width="50"><c:out value="Action"/></th>
        </tr>
        <c:forEach var="p" items="${sessionScope}">
        <tr>
        <td><c:out value="${p.key}"/></td>
        <td><c:out value="${p.value[0]}"/></td>
        <td><a href="./Reaction?action=remove&name=${p.key}&value=${p.value[0]}">delete</a></td>
        </tr>
        <c:forEach var="n" items="${p.value}" begin="1">
        <tr>
        <td>&nbsp;</td>
        <td><c:out value="${n}"/></td>
        <td><a href="./Reaction?action=remove&name=${p.key}&value=${n}">delete</a></td>
        </tr>
        </c:forEach>
        </c:forEach>
        <tr>
        <td><input type="text" name="name"></td>
        <td><input type="text" name="value"></td>
        <td><input type="submit" name="action" value="add"></td>
        </tr>
        </table>
        </form>
</body>
</html>