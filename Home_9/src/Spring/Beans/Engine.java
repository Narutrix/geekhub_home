package Spring.Beans;

import org.springframework.stereotype.Component;

@Component
public class Engine implements StatusAware {
	private String str = "The engine is work!";
	private int speed = 0;

	@Override
	public String getstatus() {
		return str;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

}
