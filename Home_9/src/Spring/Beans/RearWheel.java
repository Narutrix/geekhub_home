package Spring.Beans;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class RearWheel extends Wheel implements StatusAware, Retarding {
	@Autowired
	private GasTank f;
	@Autowired
	private Engine e;

	@Override
	public void retarding(int k) {
		int fuel = f.getFuel();
		int speed = e.getSpeed();
		if ((speed - k) > 0) {
			e.setSpeed(speed - k);
		} else {
			e.setSpeed(0);
		}
		f.setFuel(fuel - k);
	}

	@Override
	public String getstatus() {
		return "rearwheel is work";
	}

}
