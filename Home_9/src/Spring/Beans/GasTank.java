package Spring.Beans;

import org.springframework.stereotype.Component;

@Component
public class GasTank implements StatusAware {
	private int fuel = 50;

	@Override
	public String getstatus() {
		return null;
	}

	public int getFuel() {
		return fuel;
	}

	public void setFuel(int fuel) {
		this.fuel = fuel;
	}

}
