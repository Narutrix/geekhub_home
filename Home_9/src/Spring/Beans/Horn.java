package Spring.Beans;
import org.springframework.stereotype.Component;


@Component
public class Horn implements StatusAware {

	@Override
	public String getstatus() {
		return "horn is work!";
	}

	public String Beep() {
		return "Beep, Beepppp \n";
	}
}
