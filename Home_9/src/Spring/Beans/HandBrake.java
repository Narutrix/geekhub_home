package Spring.Beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class HandBrake implements StatusAware {

	@Override
	public String getstatus() {		
		return "HandBrake is work!";
	}
	@Autowired
	private Engine e;
	public void handbrake(){
		e.setSpeed(0);
	}

}
