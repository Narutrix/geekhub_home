package Spring.Beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class ForwardWheel extends Wheel implements Retarding, StatusAware,
		Steared {
	@Autowired
	private Engine e;
	@Autowired
	private GasTank f;

	@Override
	public void turn(int t) {
		int speed = e.getSpeed();
		int fuel = f.getFuel();
		if (fuel == 0) {
			System.out.println("Man you should fill your car!");
			return;
		}
		if (t > 90) {
			System.out.println("large angle");
			return;
		}
		if (t > 0 && t < 15) {
			f.setFuel(fuel - (int) speed * (t / 10));
			fuel = f.getFuel();
		}
		if (t > 15 && t < 30) {
			f.setFuel(fuel - (int) speed * (t / 10));
			fuel = f.getFuel();
		}
		if (t > 30 && t < 45) {
			f.setFuel(fuel - (int) speed * (t / 10));
			fuel = f.getFuel();
		}
		if (t > 45 && t < 60) {
			f.setFuel(fuel - (int) speed * (t / 10));
			fuel = f.getFuel();
		}
		if (t > 60 && t < 75) {
			f.setFuel(fuel - (int) speed * (t / 10));
			fuel = f.getFuel();
		}
		if (t > 75 && t <= 90) {
			f.setFuel(fuel - (int) speed * (t / 10));
			fuel = f.getFuel();
		}
	}

	@Override
	public String getstatus() {
		return "forward wheel is work!";
	}

	@Override
	public void retarding(int k) {
		int fuel = f.getFuel();
		int speed = e.getSpeed();
		if ((speed - k) > 0) {
			e.setSpeed(speed - k);
		} else {
			e.setSpeed(0);
		}
		f.setFuel(fuel - k);
	}

}
