package Spring.Beans;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Accelerator implements StatusAware {
	@Autowired
	Engine e;
	@Autowired
	GasTank f;

	@Override
	public String getstatus() {
		return "accelerator is work!";
	}

	public void accelerator(int acc) {
		int fuel = f.getFuel();
		int speed = e.getSpeed();
		if (fuel == 0) {
			System.out.println("Man you should fill your car!");
			return;
		}
		if ((speed + acc) < 320)
			e.setSpeed(speed + acc);
		else {
			e.setSpeed(320);
			System.out.print("your speed is greater than 320 is dangerous");
		}
		f.setFuel(fuel - 10);
	}
}
