package Spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import Spring.Beans.Accelerator;
import Spring.Beans.Engine;
import Spring.Beans.ForwardWheel;
import Spring.Beans.GasTank;
import Spring.Beans.HandBrake;
import Spring.Beans.Horn;
import Spring.Beans.RearWheel;

@Component
public class ControlPanel implements Vehicle {
	@Autowired
	public Engine engine;
	@Autowired
	public GasTank gasTank;
	@Autowired
	public ForwardWheel forwardWheel;
	@Autowired
	public Horn horn;
	@Autowired
	public HandBrake handBrake;
	@Autowired
	public Accelerator accelerator;
	@Autowired
	public RearWheel rearWheel;

	@Override
	public void getInfo() {
		int speed = engine.getSpeed();
		int fuel = gasTank.getFuel();
		System.out.printf("Fuel = "+ fuel + "\n");
		System.out.printf("Speed = "+ speed+ "\n");
		engine.getstatus();
		gasTank.getstatus();
		forwardWheel.getstatus();
		horn.getstatus();
		handBrake.getstatus();
		accelerator.getstatus();
		rearWheel.getstatus();
		if (fuel == 0) {
			System.out.println("Man you should fill your car!");
		}
	}

	@Override
	public void accelerator(int acc) {
		accelerator.accelerator(acc);

	}

	@Override
	public void retarding(int k) {
		forwardWheel.retarding(k);
		rearWheel.retarding(k);

	}

	@Override
	public void Beep() {
		horn.Beep();
	}

	@Override
	public void turn(int t) {
		forwardWheel.turn(t);
	}

	@Override
	public void handBrake() {
		handBrake.handbrake();
	}

	@Override
	public void charging(int fuel) {
		gasTank.setFuel(fuel);
	}

}
