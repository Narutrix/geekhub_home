package Spring;

public interface Vehicle {
	public void accelerator(int acc);
    public void retarding(int k);
    public void Beep();
    public void turn(int t);
    public void handBrake();
    public void charging(int fuel);
    public void getInfo();

}
