package Spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class EnteryPoint {


	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Vehicle vehicle = context.getBean(ControlPanel.class);

       vehicle.getInfo();
        vehicle.accelerator(100);
        vehicle.getInfo();
        vehicle.Beep();
        vehicle.getInfo();
        vehicle.turn(20);
        vehicle.getInfo();
        vehicle.retarding(30);
        vehicle.getInfo();
        vehicle.handBrake();
        vehicle.getInfo();
	}

}
