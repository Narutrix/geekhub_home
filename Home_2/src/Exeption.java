class Exception1 extends Exception {
        private int Error;
        public Exception1(int er)
        {
                Error = er;
        }
        public String ToString()
        {
                return "Exception son error at number " + Error;
        }       
}

class Exception2 extends RuntimeException {
        private int Error;
        public Exception2(int er)
        {
                Error = er;
        }
        public String ToString()
        {
                return "Runtime exception son error at number " + Error;
        }
}