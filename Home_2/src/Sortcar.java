
public class Sortcar implements Comparable<Sortcar> {
 private String markcar;
 private Integer lbase;
 private String color;
 
 public Sortcar (String tcar,Integer leibase, String ncolor){
	 markcar = tcar;
	 lbase = leibase;
	 color = ncolor;
 }
 
 public boolean equals(Sortcar o) {
	 return (markcar.equals(o.markcar)) && (color.equals(o.color)) && (lbase.equals(o.lbase));
 }

@Override
public int compareTo(Sortcar o) {
	if(color.compareTo(o.color) != 0 ) 
		return color.compareTo(o.color);
	if(markcar.compareTo(o.markcar)  != 0) 
		return markcar.compareTo(o.markcar);
	if(o.lbase.compareTo(lbase) != 0)
		return o.lbase.compareTo(lbase);
	return 0;
}
public String toString() {
    return color + " " + markcar + ", " + lbase.toString();
}
}
