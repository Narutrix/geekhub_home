import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class Stack<T> implements Queue<T> {
 private LinkedList<T> mylist = new LinkedList<>();

@Override
public boolean addAll(Collection<? extends T> c) {	
	return mylist.addAll(c);
}

@Override
public void clear() {
	mylist.clear();
}

@Override
public boolean contains(Object o) {
	return mylist.contains(o);
}

@Override
public boolean containsAll(Collection<?> c) {
	return mylist.containsAll(c);
}

@Override
public boolean isEmpty() {
	return mylist.isEmpty();
}

@Override
public Iterator<T> iterator() {
	return mylist.iterator();
}

@Override
public boolean remove(Object o) {
	return mylist.remove(o);
}

@Override
public boolean removeAll(Collection<?> c) {
	return mylist.removeAll(c);
}

@Override
public boolean retainAll(Collection<?> c) {
	return mylist.retainAll(c);
}

@Override
public int size() {
	return mylist.size();
}

@Override
public Object[] toArray() {
	return mylist.toArray();
}

@Override
public <T> T[] toArray(T[] a) {
	return mylist.toArray(a);
}

@Override
public boolean add(T e) {
	return mylist.add(e);
}

@Override
public T element() {
	return mylist.element();
}

@Override
public boolean offer(T e) {
	return mylist.offer(e);
}

@Override
public T peek() {
	return mylist.peekLast();
}

@Override
public T poll() {
	return mylist.pollLast();
}

@Override
public T remove() {
	return mylist.removeLast();
}
}