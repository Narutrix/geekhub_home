import java.awt.*;
import java.util.*;

public class Cat {

	private int[] rgbcolor = new int[3];
	private int age;

	public Cat(Color color, int newage) {
		rgbcolor[0] = color.getBlue();
		rgbcolor[1] = color.getGreen();
		rgbcolor[2] = color.getRed();
		age = newage;
	}

	public String toString() {
		return "Age = " + Integer.toString(age) + " color = "
				+ Arrays.toString(rgbcolor);
	}

	public int hashCode() {
		return age * 128 * 456 * 231 + rgbcolor[0] * 73346 * 436 + rgbcolor[1]
				* 25326 + rgbcolor[2] * 9;

	}

	public boolean equals(Object othercat) {
		if (this == othercat) {
			return true;
		}
		if (othercat instanceof Cat) {
			return this.rgbcolor[0] == ((Cat) othercat).rgbcolor[0]
					&& rgbcolor[1] == ((Cat) othercat).rgbcolor[1]
					&& rgbcolor[2] == ((Cat) othercat).rgbcolor[2]
					&& age == ((Cat) othercat).age;
		}
		return false;
	}

	public static void main(String[] args) {
		Cat murchik = new Cat(Color.blue, 6);
		Cat masha = new Cat(Color.blue, 6);
		System.out.println(masha.equals(murchik));

	}
}