import java.util.LinkedList;
import java.util.Collection;
import java.util.Iterator;

public class BallotBox implements Collection<Vote>  {
 	private LinkedList<Vote> spisok = new LinkedList<>();
 	private String const1;
 	private String const2;
 	BallotBox (String newconst1, String newconst2){
 		const1 = newconst1;
 		const2 = newconst2;
 	}
 	 	
 	public void addbox(Vote people){
 		if (people.toString() == const2 ){
 			spisok.add(people);
 			spisok.add(people);
 		}
 		else if (people.toString() == const1 ){
 			spisok.add(people);
 		}
 	}

	@Override
	public boolean add(Vote e) {
		return spisok.add(e);
	}

	@Override
	public boolean addAll(Collection<? extends Vote> c) {
		return spisok.addAll(c);
	}

	@Override
	public void clear() {
		spisok.clear();
		
	}

	@Override
	public boolean contains(Object o) {
		return spisok.contains(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return spisok.containsAll(c);
	}

	@Override
	public boolean isEmpty() {
		return spisok.isEmpty();
	}

	@Override
	public Iterator<Vote> iterator() {
		return spisok.iterator();
	}

	@Override
	public boolean remove(Object o) {
		return spisok.remove(o);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return spisok.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return spisok.retainAll(c);
	}

	@Override
	public int size() {
		return spisok.size();
	}

	@Override
	public Object[] toArray() {
		return spisok.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return spisok.toArray(a);
	}


}

