import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.*;

public class BeanRepresenter {

	private List<Object> visit;

	public BeanRepresenter() {
		visit = new ArrayList<Object>();
	}

	private void ShowArray(Object arr, String space) {
		if (arr == null || !arr.getClass().isArray())
			return;
		System.out.print(space + "[ ");
		for (int i = 0; i < Array.getLength(arr); i++) {
			System.out.print(Array.get(arr, i).toString());
			if (i != Array.getLength(arr) - 1) {
				System.out.print(", ");
			}
		}
		System.out.println(" ]");
	}

	public void Show(Object object, String space)
			throws IllegalArgumentException, IllegalAccessException {
		if (object == null || visit.contains(object))
			return;
		visit.add(object);
		Class clazz = object.getClass();
		System.out.println(space + "Class " + clazz.getName());
		Field[] objectFields = clazz.getDeclaredFields();
		for (Field field : objectFields) {
			field.setAccessible(true);
			Class fieldType = field.getType();

			if (fieldType.isPrimitive() || fieldType == String.class) {
				System.out.println(space + field.getName() + ": "
						+ field.get(object));
			} else if (fieldType.isArray()) {
				System.out.println(space + field.getName() + ": ");
				ShowArray(field.get(object), space);
			} else if (!fieldType.isPrimitive()) {
				Show(field.get(object), space + "|");
			}
		}
	}
}
