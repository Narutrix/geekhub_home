import java.awt.*;
import java.util.*;

public class Cat {

	private int[] rgbcolor = new int[3];
	LinkedList<Integer> Llist = new LinkedList<>();
	private int age;
	private String Name = "joi";

	public Cat(Color color, int newage) {
		rgbcolor[0] = color.getBlue();
		rgbcolor[1] = color.getGreen();
		rgbcolor[2] = color.getRed();
		age = newage;
		Llist.add(0);
		Llist.add(1);
		Llist.add(2);
		Llist.add(3);
	}

	public String toString() {
		return "Age = " + Integer.toString(age) + " color = "
				+ Arrays.toString(rgbcolor);
	}

	public int hashCode() {
		return age * 128 * 456 * 231 + rgbcolor[0] * 73346 * 436 + rgbcolor[1]
				* 25326 + rgbcolor[2] * 9;

	}

	public boolean equals(Cat othercat) {
		if (this == othercat) {
			return true;
		}
		if (othercat instanceof Cat) {
			return rgbcolor[0] == othercat.rgbcolor[0]
					&& rgbcolor[1] == othercat.rgbcolor[1]
					&& rgbcolor[2] == othercat.rgbcolor[2]
					&& age == othercat.age;
		}
		return false;
	}


}